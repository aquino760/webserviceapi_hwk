﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var dictionaryItem = DictionaryItem.FromJson(jsonString);

namespace WebServiceAPI_HWK.Models

{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class DictionaryItem
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string Definition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }
    }

    public partial class DictionaryItem
    {
        public static List<DictionaryItem> FromJson(string json) => JsonConvert.DeserializeObject<List<DictionaryItem>>(json, WebServiceAPI_HWK.Models.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this DictionaryItem[] self) => JsonConvert.SerializeObject(self, WebServiceAPI_HWK.Models.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
