﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Xamarin.Forms;
using Plugin.Connectivity;
using Xamarin.Forms.Xaml;
using WebServiceAPI_HWK.Models;
using ModernHttpClient;
namespace WebServiceAPI_HWK
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DictionaryPage : ContentPage
	{
		public DictionaryPage ()
		{
			InitializeComponent ();
		}

        //checking internet connection
        private bool CheckConnection()
        {
            bool internetStatus;
            if (CrossConnectivity.Current.IsConnected == true)
                internetStatus = true;
            else
                internetStatus = false;

            return internetStatus;
        }

        private void goodConnectionText()
        {
            ConnectionStatusLabel.Text = "Connected to Internet via " +
                    CrossConnectivity.Current.ConnectionTypes.First();
        }

        private void badConnectionText()
        {
            ConnectionStatusLabel.Text = "Not Connected to Internet";
        }

        void Handle_ClickedCheckConnection(object sender, System.EventArgs e)
        {
            if (CheckConnection())
            {
                goodConnectionText();
            }
            else
            {
                badConnectionText();
            }
        }

        //calling the search function
        void HandleSearchWord(object sender, System.EventArgs e)
        {
            string word = ((Entry)sender).Text;
            if (CheckConnection())
            {
                goodConnectionText();
                GetDictionary_Clicked(word);
            }
            else
            {
                DisplayAlert("Warning","You are not connected","Ok");
                badConnectionText();
            }
        }

        async void GetDictionary_Clicked(string word)
        {
            //HttpClient client = new HttpClient();
            var client = new HttpClient(new NativeMessageHandler());
            string keyword = word.ToLower();

            var uri = new Uri(
                string.Format($"https://owlbot.info/api/v2/dictionary/"+ 
                              $"{keyword}"));

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            request.Headers.Add("Application", "application / json");

            HttpResponseMessage response = await client.SendAsync(request);
            List<DictionaryItem> DictionaryApiData = null;
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                if (content == "[]")
                {
                    Type.Text = "Unknown";
                    Definition.Text = "Please try another";
                    Example.Text = " ";
                }
                else
                {
                    DictionaryApiData = DictionaryItem.FromJson(content);
                    Type.Text = "Type: " + DictionaryApiData[0].Type;
                    Definition.Text = "Definition: " + DictionaryApiData[0].Definition;
                    Example.Text = "Example: " + DictionaryApiData[0].Example;
                }
            }
        }

    }
}